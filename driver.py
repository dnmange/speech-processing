'''
driver assignment 2
Name:
'''

import mydsp.plots as plots
import matplotlib.pyplot as plt
import numpy as np
import mydsp.utils as utils
import os

from mydsp.multifileaudioframes import MultiFileAudioFrames
from mydsp.dftstream import DFTStream
from mydsp.pca import PCA
from mydsp.gaussian_mixture_speech import GaussionModel


def plot_narrowband_wideband(filename):
    """plot_narrowband_widband(filename)
    Given a speech file, display narrowband and wideband
    spectrograms of the speech.
    """
    filelist = [filename]

    dft_db_wideb, taxis_wideb, faxis_wideb = plots.spectrogram(filelist, adv_ms=10, len_ms=20)
    dft_db_narowb, taxis_narowb, faxis_narowb = plots.spectrogram(filelist, adv_ms=20, len_ms=40)

    dft_db_narowb = np.transpose(dft_db_narowb)
    dft_db_wideb = np.transpose(dft_db_wideb)

    fig = plt.figure()

    # wide band plot
    plt.subplot(211)
    plt.pcolormesh(taxis_wideb, faxis_wideb, dft_db_wideb)
    plt.title("Wide band Spectogram")
    plt.xlabel('time in sec')
    plt.ylabel('freq. in Hz')
    fig.text(.5, 0.5, "Wide band has poorer frequency resolution", ha='center')
    plt.colorbar()

    # narrow band plot
    plt.subplot(212)
    plt.pcolormesh(taxis_narowb, faxis_narowb, dft_db_narowb)
    plt.title("Narrow band Spectogram")
    plt.xlabel('time in sec')
    plt.ylabel('freq. in Hz')
    fig.text(.5, 0.02, "Narrow band has poorer time resolution", ha='center')
    plt.colorbar()
    plt.tight_layout()
    plt.show()

    pass


def pca_analysis(corpus_dir):
    """pca_analysis(corpus_dir)
    Given a directory containing a speech corpus, compute spectral features
    and conduct a PCA analysis of the data.
    """
    dft_list = create_dft_stream(corpus_dir)

    # apply pca on corpus_dir
    pca = PCA(dft_list, corr_anal=False)

    '''
    how much variance is captured is determined by 
    cumulative sum of m dimesions divide by total sum of eigen val
    '''
    variance_captured = (np.cumsum(pca.eigen_val) / np.sum(pca.eigen_val))

    # plot variance vs no of components
    fig = plt.figure()
    plt.plot(variance_captured)
    plt.xlabel('Number of components')
    plt.ylabel('Variance')
    fig.text(.5, 0.02, "Number of PCA components used versus the amount of variance captured.", ha='center')
    plt.show()

    # compute no of component in decile
    no_of_component_in_decile = []
    i = 1
    variance_index = 0

    for variance in variance_captured:
        while i <= np.floor(variance * 10) and i <= 10:
            no_of_component_in_decile.append(variance_index + 1)
            i += 1
            continue

        if i > 10:
            break

        variance_index += 1

    # print no_of_components w.r.t decile
    counter = 0
    while counter < len(no_of_component_in_decile):
        print(
            "In Decile {:d}, no of components are: {:d}".format(10 * (counter + 1), no_of_component_in_decile[counter]))
        counter += 1

    # applying pca transform on single file
    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "woman/ac/6a.wav")
    dft_list = create_dft_stream([file_path])
    pca_transformed = pca.transform(dft_list, no_of_component_in_decile[5])

    # compute time axis
    time_axis = create_time_axis(len(pca_transformed), 10)

    pca_transformed = np.abs(np.transpose(pca_transformed))
    pca_transformed_axis = []

    for pca_comp in range(0, len(pca_transformed)):
        pca_transformed_axis.append(pca_comp)

    # computing PCA comp as Y axis
    pca_transformed_axis = np.array(pca_transformed_axis)

    # plotting the spectrogram with new values in PCA space
    fig = plt.figure()
    plt.pcolormesh(time_axis, pca_transformed_axis, pca_transformed)
    plt.title("Spectrogram ")
    plt.xlabel('time in sec')
    plt.ylabel('Components')
    fig.text(.5, 0.02, "This spectrogram represents new values in PCA space", ha='center')
    plt.colorbar()
    plt.tight_layout()
    plt.show()
    pass


def create_dft_stream(files):
    '''
    creates dft stream from provided files list
    :return: dft stream in list
    '''
    dft_list = []
    audio_frame = MultiFileAudioFrames(files, 10, 20)
    dft_stream = DFTStream(audio_frame)

    for dft in dft_stream:
        dft_list.append(dft)

    return dft_list


def create_time_axis(length, adv_ms):
    '''
    creates time axis according to length and advance_ms
    :return: time axis in list format
    '''
    taxis = []
    counter = 0
    start = 0

    # compute time axis
    while counter < length:
        taxis.append(start)
        start += adv_ms / 1000
        counter += 1

    return taxis


def speech_silence(filename):
    """speech_silence(filename)
    Given speech file filename, train a 2 mixture GMM on the
    RMS intensity and label each frame as speech or silence.
    Provides a plot of results.
    """

    # creates a gaussian mixture model for given parameters
    gaussian_model = GaussionModel(filename=filename, adv_ms=10, len_ms=20)
    gaussian_model.execute()
    pass


if __name__ == '__main__':
    # If we are here, we are in the script-level environment; that is
    # the user has invoked python driver.py.  The module name of the top
    # level script is always __main__

    # problem 2
    # call plot_narrowband_wideband

    filename = "em-gmm_input/shaken.wav"
    plot_narrowband_wideband(filename=filename)

    # problem 3
    # Call pca_analysis
    dir = "woman/"
    pca_analysis(utils.get_corpus(dir))

    # problem 4
    # call speech_silence
    speech_silence(filename=filename)
    pass  # for breakpoint if desired
