'''
gaussian mixture model on speech data
'''

import numpy as np
import matplotlib.pyplot as plt

from mydsp.audioframes import AudioFrames
from mydsp.rmsstream import RMSStream
from sklearn.mixture import GaussianMixture

'''
Applies Gaussian mixture model on speech and noise data
'''


class GaussionModel(object):

    def __init__(self, filename, adv_ms, len_ms):
        self.filename = filename
        self.adv_ms = adv_ms
        self.len_ms = len_ms

    def input(self):
        '''
        prepare input for Gaussian mixture model
        :return:
        '''
        # getting audio frames from specified params
        self.audio_frames = AudioFrames(filename=self.filename, adv_ms=self.adv_ms, len_ms=self.len_ms)

        # compute rms stream for the same
        self.rmss_stream = RMSStream(self.audio_frames)

        streams = []

        # as rmss stream is object not array
        for stream in self.rmss_stream:
            streams.append(stream)

        self.rmss_stream = streams
        return np.reshape(np.asarray(streams), (-1, 1))

    def execute(self):
        '''
        fit Gaussian model on rms stream and predict on the same,
        seperate speech which corresponds to higher mean and noise
        corresponds to lower mean
        :return:
        '''

        input_data = self.input()
        gmm = GaussianMixture(n_components=2)
        gmm.fit(input_data)

        # predict assigns labels on index
        labels = gmm.predict(input_data)

        time_axis = self.create_time_axis()

        speech_data, noise_data, time_for_speech, time_for_noise = \
            self.split_axis_by_label(time_axis=time_axis, input_data=input_data, label=labels, model=gmm)

        # plot speech and noise data separately
        plt.ion()
        fig = plt.figure()

        plt.scatter(time_for_speech, speech_data, marker='x', label='Speech')
        plt.scatter(time_for_noise, noise_data, marker='o', label='Noise')

        plt.title("RMS Intensity vs Time")
        plt.xlabel("time in sec")
        plt.ylabel('dB')
        plt.legend()
        fig.text(.5, 0.02, "Distribution of speech and noise data on RMSS stream", ha='center')
        plt.show()

    def create_time_axis(self):
        '''
        creates time axis from frame advance and rmss stream
        :return:
        '''

        time_axis = []
        counter = 0
        start = 0

        while counter < len(self.rmss_stream):
            time_axis.append(start)
            start += self.audio_frames.get_frameadv_ms() / 1000
            counter += 1

        return time_axis

    def split_axis_by_label(self, time_axis, input_data, label, model):
        '''
        To represent marker "x" and "o", input_data is split according
        to the labels

        :param time_axis: time in sec
        :param input_data: input is rmss stream
        :param label: labels is used to split the data in speech and noise
        :param model: gmm model

        :return: sepearate data and time axis in speech and noise
        '''

        noise_data = []
        speech_data = []
        time_for_speech = []
        time_for_noise = []

        [x_mean, y_mean] = model.means_

        # assign label for speech
        if x_mean > y_mean:
            speech = 0
        else:
            speech = 1

        for i in range(0, label.size):
            if label[i] == speech:
                speech_data.append(input_data[i])
                time_for_speech.append(time_axis[i])
            else:
                noise_data.append(input_data[i])
                time_for_noise.append(time_axis[i])

        np.asarray(noise_data)
        np.asarray(speech_data)
        np.asarray(time_for_speech)
        np.asarray(time_for_noise)

        return [speech_data, noise_data, time_for_speech, time_for_noise]
