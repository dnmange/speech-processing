'''
Created on Sep 6, 2017

@author: mroch
'''

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from mpl_toolkits.mplot3d import Axes3D


def genData(N, dim):
    """genData(N, dim) - Return N samples of dim dimensional normal data at random positions
        returns(data, mu, var)  where mu and var are the mu/var for each dimension
    """
    # Generate random parameters 
    mu = np.random.rand(dim) * 50
    mu[2] = mu[2] + 70  # Raise z mean so above projection
    var = np.array([50, 100, 5]) + np.random.rand(dim) * 10

    # Build up random variance-covariance matrix
    varcov = np.zeros([dim, dim])
    # Fill off diagonals in lower triangle
    for i in range(1, dim):
        varcov[i, 0:i] = np.random.rand(i) * 5
    # make symmetric
    varcov = varcov + np.transpose(varcov)
    # add in variances
    varcov = varcov + np.diag(var)

    data = np.random.multivariate_normal(mu, varcov, N)
    return (data, mu, varcov)


class PCA(object):
    '''
    PCA
    '''

    def __init__(self, data, corr_anal=False):
        '''
        PCA(data)
        data matrix is N x Dim
        Performs variance-covariance matrix or correlation matrix
        based principal components analysis of detrended (by
        mean removal) data.
        
        If the optional corr_anal is True, correlation-based PCA
        is performed
        '''

        # You are not required to implement corr_anal == True case
        # but it's pretty easy to do once you have the variance-
        # covariance case done

        substracted_mean = sig.detrend(data, axis=1, type="constant")

        self.covariance_mat = np.cov(substracted_mean, rowvar=False)
        self.eigen_val, self.eigen_vec = np.linalg.eig(self.covariance_mat)
        self.dim = self.covariance_mat.shape[0]

        # sort according to eigen values in descending order
        index = self.eigen_val.argsort()[::-1]

        self.eigen_val = self.eigen_val[index]

        # get abs value as eigen values are complex
        self.eigen_val = np.abs(self.eigen_val)

        self.eigen_vec = self.eigen_vec[:, index]

    def get_pca_directions(self):
        """get_pca_directions() - Return matrix of PCA directions
        Each column is a PCA direction, with the first column
        contributing most to the overall variance.
        """
        return self.eigen_vec

    def transform(self, data, dim=None):
        """transform(data, dim) - Transform data into PCA space
        To reduce the dimension of the data, specify dim as the remaining 
        number of dimensions. Omitting dim results in using all PCA axes 
        """
        if not dim:
            return np.dot(data, self.eigen_vec[:, 0:self.dim])  # use all dimensions if not specified

        return np.dot(data, self.eigen_vec[:, 0:dim])

    def get_component_loadings(self):
        """get_component_loadings()
        Return a square matrix of component loadings. Column j shows the amount
        of variance from each variable i in the original space that is accounted
        for by the jth principal component
        """
        component_loadings = np.zeros([self.dim, self.dim])
        self.standard_deviation = np.sqrt(np.diag(self.covariance_mat))

        for x in range(self.dim):
            try:
                component_loadings[:, x] = self.eigen_vec[:, x] * np.sqrt(self.eigen_val[x]) / self.standard_deviation
            except RuntimeWarning as e:
                print(e)

        return component_loadings


if __name__ == '__main__':

    plt.ion()  # Interactive plotting

    # Demonstration of using PCA

    # Generate synthetic data set and plot it
    (data, mu, Sigma) = genData(500, 3)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    h_data = ax.scatter(data[:, 0], data[:, 1], data[:, 2])
    ax.set_xlabel('$f_0$')
    ax.set_ylabel('$f_1$')
    ax.set_zlabel('$f_2$')

    # Conduct PCA analysis using variance-covariance method  
    pca = PCA(data)

    # Show the PCA directions 
    k = 10  # scale up unit vectors by k 
    v = pca.get_pca_directions()
    for idx in range(v.shape[1]):
        ax.quiver(0, 0, 0, k * v[idx, 0], k * v[idx, 1], k * v[idx, 2],
                  color='xkcd:orange')

    # project onto 2d 
    vc_proj = pca.transform(data, 2)
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    h_vc_proj = ax.scatter(vc_proj[:, 0], vc_proj[:, 1], color='xkcd:orange')

    # repeat w/ autocorrelation analysis
    pcaR = PCA(data, corr_anal=True)

    w = pcaR.get_pca_directions()
    for idx in range(v.shape[1]):
        ax.quiver(0, 0, 0, k * w[idx, 0], k * w[idx, 1], k * w[idx, 2], color='xkcd:lilac')

    # project onto 2d 
    r_proj = pcaR.transform(data, 2)
    h_r_proj = ax.scatter(r_proj[:, 0], r_proj[:, 1], color='xkcd:lilac')

    # Add legend.  $...$ enables LaTeX-like equation formatting
    plt.legend([h_data, h_vc_proj, h_r_proj],
               ['Original data', '$\Sigma $projection', '$R$ projection'])
    print("Autocorrelation component loadings")
    print(pcaR.get_component_loadings())

    x = 3  # breakable line so our windows don't go away
