import numpy as np

from mydsp.multifileaudioframes import MultiFileAudioFrames
from mydsp.dftstream import DFTStream
import matplotlib.pyplot as plt


def spectrogram(files, adv_ms, len_ms, plot=False):
    """spectrogram(files, adv_ms, len_ms, plot)
    Given a list of files and framing parameters (advance, length in ms), 
    compute a spectrogram that spans the files.  
    
    Returns [intensity_dB, taxis, faxis]
    
    If optional plot is True (default False), the spectrogram is plotted
    """

    audio_frames = MultiFileAudioFrames(filelist=files, adv_ms=adv_ms, len_ms=len_ms)
    dft_stream = DFTStream(frame_stream=audio_frames)

    dft_stream_db = np.array(dft_stream.stream)
    taxis = []
    counter = 0
    start = 0

    # compute time axis
    while counter < len(dft_stream_db):
        taxis.append(start)
        start += adv_ms / 1000
        counter += 1

    # compute frequency axis
    faxis = dft_stream.get_Hz()

    if plot == True:
        plot_spectogram(dft_stream_db, taxis, faxis, len_ms)
    return [dft_stream_db, taxis, faxis]


def plot_spectogram(dft_stream_db, taxis, faxis, len_ms):
    '''
    Plots spectogram acc to specified window length
    :param dft_stream_db: Intensity
    :param taxis: time axis representation
    :param faxis: frequency axis representation
    :param len_ms: window length
    :return:
    '''

    fig = plt.figure()
    plt.pcolormesh(taxis, faxis, np.transpose(dft_stream_db))
    plt.title("Spectogram")
    plt.xlabel('time in sec')
    plt.ylabel('freq. in Hz')
    fig.text(.5, 0.02, "Spectogram having window length as: {:d} ms".format(len_ms), ha='center')
    plt.colorbar()